<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Str;
class IdentificationNumber extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        for ($i=1; $i < 30; $i++) { 
            
            User::create([
                'id' => $i,
                'name' => Str::random(8),
                'email' => Str::random(12).'@mail.com',
                'password' => bcrypt('12345678'),
                'identification_number' => mt_rand(100000000, 999999999),
                'type' => 'default',
                'api_token' => Str::random(80)
            ]);
        }
    }
}
