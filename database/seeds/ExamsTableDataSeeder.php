<?php

use Illuminate\Database\Seeder;
use App\Exam;

class ExamsTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $proffession = array("Maths","English","History","Medicine","Biology","Computers");
        for ($i=0; $i < 30; $i++) { 
            $randIndex = array_rand($proffession);
	    	Exam::create([
	            'grade' => rand(0,100),
                'student_id' => $i,
                'proffession' => $proffession[$randIndex]
	        ]);
    	}
    }
}
