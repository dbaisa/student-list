-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 08, 2020 at 07:11 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `blog4`
--

-- --------------------------------------------------------

--
-- Table structure for table `exams`
--

CREATE TABLE `exams` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `grade` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `proffession` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Math'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `exams`
--

INSERT INTO `exams` (`id`, `grade`, `student_id`, `created_at`, `updated_at`, `proffession`) VALUES
(65, 57, 0, '2020-04-04 12:21:55', '2020-04-04 12:21:55', 'Biology'),
(107, 7, 0, '2020-04-06 07:30:00', '2020-04-06 07:30:00', 'Medicine'),
(123, 59, 16, '2020-04-06 07:30:00', '2020-04-06 07:30:00', 'Medicine'),
(124, 98, 17, '2020-04-06 07:30:00', '2020-04-06 07:30:00', 'Maths'),
(125, 35, 18, '2020-04-06 07:30:00', '2020-04-06 07:30:00', 'Medicine'),
(126, 56, 19, '2020-04-06 07:30:00', '2020-04-06 07:30:00', 'Computers'),
(127, 25, 20, '2020-04-06 07:30:00', '2020-04-06 07:30:00', 'Biology'),
(128, 59, 21, '2020-04-06 07:30:00', '2020-04-06 07:30:00', 'Computers'),
(129, 36, 22, '2020-04-06 07:30:00', '2020-04-06 07:30:00', 'Maths'),
(130, 29, 23, '2020-04-06 07:30:00', '2020-04-06 07:30:00', 'Biology'),
(131, 73, 24, '2020-04-06 07:30:00', '2020-04-06 07:30:00', 'History'),
(132, 55, 25, '2020-04-06 07:30:00', '2020-04-06 07:30:00', 'English'),
(133, 28, 26, '2020-04-06 07:30:00', '2020-04-06 07:30:00', 'Computers'),
(134, 65, 27, '2020-04-06 07:30:00', '2020-04-06 07:30:00', 'Computers'),
(135, 12, 28, '2020-04-06 07:30:00', '2020-04-06 07:30:00', 'History'),
(136, 43, 29, '2020-04-06 07:30:00', '2020-04-06 07:30:00', 'Maths'),
(137, 28, 26, '2020-04-06 09:48:57', '2020-04-06 09:48:57', 'Computers'),
(138, 73, 24, '2020-04-06 09:50:46', '2020-04-06 09:50:46', 'Maths'),
(139, 28, 26, '2020-04-06 09:51:50', '2020-04-06 09:51:50', 'English'),
(140, 28, 26, '2020-04-06 09:56:41', '2020-04-06 09:56:41', 'History'),
(141, 65, 27, '2020-04-06 09:57:28', '2020-04-06 09:57:28', 'Biology'),
(142, 99, 26, '2020-04-06 10:04:49', '2020-04-06 10:04:49', 'Computers'),
(144, 65, 27, '2020-04-06 10:10:09', '2020-04-06 10:10:09', 'Computers'),
(147, 78, 26, '2020-04-06 10:19:34', '2020-04-06 10:19:34', 'English'),
(150, 100, 47, '2020-04-07 19:05:04', '2020-04-07 19:05:04', 'Computers'),
(151, 200, 47, '2020-04-07 19:06:48', '2020-04-07 19:06:48', 'Computers'),
(152, 100, 47, '2020-04-07 19:11:33', '2020-04-07 19:11:33', 'Computers'),
(153, 100, 47, '2020-04-07 19:12:54', '2020-04-07 19:12:54', 'English'),
(154, 1, 47, '2020-04-07 19:14:25', '2020-04-07 19:14:25', 'English'),
(155, 1, 47, '2020-04-07 19:15:28', '2020-04-07 19:15:28', 'Biology'),
(164, 14, 60, '2020-04-08 12:49:19', '2020-04-08 12:49:19', 'Maths'),
(165, 100, 61, '2020-04-08 12:54:07', '2020-04-08 12:54:07', 'Computers'),
(166, 99, 61, '2020-04-08 12:59:58', '2020-04-08 12:59:58', 'Medicine'),
(167, 98, 61, '2020-04-08 13:27:49', '2020-04-08 13:27:49', 'Biology'),
(168, 88, 61, '2020-04-08 13:29:30', '2020-04-08 13:29:30', 'Computers'),
(169, 78, 61, '2020-04-08 13:30:45', '2020-04-08 13:30:45', 'Biology');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_04_02_072301_add_token_to_users_table', 2),
(5, '2016_06_01_000001_create_oauth_auth_codes_table', 3),
(6, '2016_06_01_000002_create_oauth_access_tokens_table', 3),
(7, '2016_06_01_000003_create_oauth_refresh_tokens_table', 3),
(8, '2016_06_01_000004_create_oauth_clients_table', 3),
(9, '2016_06_01_000005_create_oauth_personal_access_clients_table', 3),
(10, '2020_04_02_115842_create_exams_table', 4),
(11, '2020_04_02_115938_add_identification_to_users_table', 4),
(12, '2020_04_03_110158_add_type_to_users_table', 5),
(13, '2020_04_04_150621_add_proffession_to_exams_table', 6);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('005bc1ff7b4198f487e6b2c074543739fd2f35c49d56e4cd428eed9f7bbbdebb0aeb841a4c93180f', 44, 1, 'Personal Access Token', '[]', 0, '2020-04-07 04:02:43', '2020-04-07 04:02:43', '2021-04-07 07:02:43'),
('0539cc0ed32332c386d1e08664ebc9bbd6d951a8b171ab90aa5f34cb8a710bb4b02a3ec96948bca5', 8, 1, 'Personal Access Token', '[]', 0, '2020-04-02 08:28:17', '2020-04-02 08:28:17', '2021-04-02 11:28:17'),
('070fc89d847a5e7079823d76abd0e22b510af0606139ba3f7ce8dbc85bbcf904993bd2fe2a18826d', 8, 1, 'Personal Access Token', '[]', 0, '2020-04-02 08:13:36', '2020-04-02 08:13:36', '2021-04-02 11:13:36'),
('08a803025d60b5532142b377ce42c1c1de4ba7ab824b27fdd3e0986bdcd2ab286c8f90b479f34f7a', 44, 1, 'Personal Access Token', '[]', 0, '2020-04-07 03:55:58', '2020-04-07 03:55:58', '2021-04-07 06:55:58'),
('09fe30114a886b7eecf383478021d99d3efcdde4fcb3867f4c3dfd2e817f801238f16ec23be9ef18', 47, 1, 'Personal Access Token', '[]', 0, '2020-04-07 03:58:26', '2020-04-07 03:58:26', '2021-04-07 06:58:26'),
('0b7f178e3422d340e70d00f3c211ad81abd78916cb2081fe56572455d6d5f2eeb1f0ab93e79b0429', 44, 1, 'Personal Access Token', '[]', 0, '2020-04-06 21:14:44', '2020-04-06 21:14:44', '2021-04-07 00:14:44'),
('1078843d01d5705e4a9e8b87a3caca4f82be161c1b2ddb258e26ae52f352152533821244831aeab6', 44, 1, 'Personal Access Token', '[]', 0, '2020-04-06 21:21:05', '2020-04-06 21:21:05', '2021-04-07 00:21:05'),
('1799d580a4be1889453b3d21be5dd1c801c37c2d7f53830bfd036128329c370692d3669f13bcb606', 44, 1, 'Personal Access Token', '[]', 0, '2020-04-06 21:10:47', '2020-04-06 21:10:47', '2021-04-07 00:10:47'),
('196e5973d8340bfa4fcc6b2a91f1ae20960ecd0fba764ee185cc1c5c8b8f6c804b82ccbda43bedb1', 8, 1, 'Personal Access Token', '[]', 0, '2020-04-02 08:44:44', '2020-04-02 08:44:44', '2021-04-02 11:44:44'),
('1f58365e8acf993c127f05919ad957892f0821c8d4f505736caf0f4b2060e97116f67dd4bfabfa6c', 61, 1, 'Personal Access Token', '[]', 0, '2020-04-08 12:57:12', '2020-04-08 12:57:12', '2021-04-08 15:57:12'),
('2e192106632e149afa2f9c07ce0661b59652d90623233c5f2694064313d9a26b193566ef3a28d681', 8, 1, 'Personal Access Token', '[]', 0, '2020-04-02 08:26:52', '2020-04-02 08:26:52', '2021-04-02 11:26:52'),
('2e4bd45d662e33710bbfc254ac38bf6be855bdd860360979d980bc67c042854415965cc8da048741', NULL, 1, 'Personal Access Token', '[]', 0, '2020-04-04 09:33:19', '2020-04-04 09:33:19', '2021-04-04 12:33:19'),
('2eb5a42511737bec1f32e3aaa0e1e138c032743216d3a80909d7515682db2e3977951b89bdf40533', 8, 1, 'Personal Access Token', '[]', 0, '2020-04-02 08:26:35', '2020-04-02 08:26:35', '2021-04-02 11:26:35'),
('36b5348f5da22461259658b4c8ff6a1863ce91465858ef515e5bd417dbd92cff887391211ca6ea86', 44, 1, 'Personal Access Token', '[]', 0, '2020-04-07 03:54:11', '2020-04-07 03:54:11', '2021-04-07 06:54:11'),
('40e611ad5080bc6c299331d7e2514effc561d89249851bdf164a087852d2db2ab2aeed643b1983ce', 44, 1, 'Personal Access Token', '[]', 0, '2020-04-08 12:30:45', '2020-04-08 12:30:45', '2021-04-08 15:30:45'),
('4908df7909a93c3b8e54eca805b7c4bb89d6c614736b242bdbcabebcba74c3ece7a10ab5ab546cfd', 61, 1, 'Personal Access Token', '[]', 0, '2020-04-08 13:28:20', '2020-04-08 13:28:20', '2021-04-08 16:28:20'),
('4b218df9423d87ee83cd800c2c594d5a88969c7c7dc3637fcf517f4c289b799dbc096c7c251f54b7', 44, 1, 'Personal Access Token', '[]', 0, '2020-04-07 04:05:34', '2020-04-07 04:05:34', '2021-04-07 07:05:34'),
('50a88bf6ac82da47c1f1fcacd36c4de44d0ee28cfa061f21a20dc7fbab76f82bb327785c895a47ee', 44, 1, 'Personal Access Token', '[]', 0, '2020-04-06 21:17:45', '2020-04-06 21:17:45', '2021-04-07 00:17:45'),
('51bf5b9ff4812dd25842bd274d3f8d169b2863cc7ae0431af2f3ef86680cf28641d2a7530a6eb937', 47, 1, 'Personal Access Token', '[]', 0, '2020-04-07 03:57:58', '2020-04-07 03:57:58', '2021-04-07 06:57:58'),
('56189b958fcd901d4e63d1b05dd31e8b3ace0df61fae472075df5f9f3bc114c77f3454e0ffc9d69a', 8, 1, 'Personal Access Token', '[]', 0, '2020-04-02 08:15:10', '2020-04-02 08:15:10', '2021-04-02 11:15:10'),
('58f445c83aaca197ab2973de8ea4dcc713969a9a041674813d4c13a7572f8704d6f66a98306bcff0', 44, 1, 'Personal Access Token', '[]', 0, '2020-04-06 21:19:55', '2020-04-06 21:19:55', '2021-04-07 00:19:55'),
('59417dd541d07812fe99c3107652a983b439eeca5cab2706d035b51de26f7b8e9d86c2a769c86c67', 8, 1, 'Personal Access Token', '[]', 0, '2020-04-02 08:29:55', '2020-04-02 08:29:55', '2021-04-02 11:29:55'),
('5ab1d1d552d53b8f5a1e439efb35acd26313218df60d3d99201c01a59252009fc42c47f16084618d', 44, 1, 'Personal Access Token', '[]', 0, '2020-04-06 21:07:27', '2020-04-06 21:07:27', '2021-04-07 00:07:27'),
('5e0882954eeb86893b84a70d830d2b4d8a7f2b8311cf52ea847075041339dd40741ddfa1ed7bd9a3', 44, 1, 'Personal Access Token', '[]', 0, '2020-04-07 18:46:04', '2020-04-07 18:46:04', '2021-04-07 21:46:04'),
('5fcc7f7db2c2f7c5d2b9d3283da461b821b24f2c989b7b919195e1652548459f11505fae81aae8ab', 8, 1, 'Personal Access Token', '[]', 0, '2020-04-02 08:26:08', '2020-04-02 08:26:08', '2021-04-02 11:26:08'),
('636e8d3e2ff754795842e6f06b1f5e817dbf756d8acd7398d4f01d915db7923d5a4935b2beeb61ee', 44, 1, 'Personal Access Token', '[]', 0, '2020-04-07 03:55:07', '2020-04-07 03:55:07', '2021-04-07 06:55:07'),
('6a6166666ce341d707bea1fd3712b5fb2bd95cecba31e225c6f307b1bb17e59638913297aed99280', 44, 1, 'Personal Access Token', '[]', 0, '2020-04-08 09:12:24', '2020-04-08 09:12:24', '2021-04-08 12:12:24'),
('6c65df22e5e2c4e8bd56ae7f400d31495eaf472c5c9ae13f800bd9c29c5e78eed9aea82072479bd3', 47, 1, 'Personal Access Token', '[]', 0, '2020-04-07 04:11:49', '2020-04-07 04:11:49', '2021-04-07 07:11:49'),
('6ddf41d4f38d2397495f2a9bce9e31cc8979a25153eb9181f67987538620f5ec4f1a3d8bf2abed83', NULL, 1, 'Personal Access Token', '[]', 0, '2020-04-04 10:04:10', '2020-04-04 10:04:10', '2021-04-04 13:04:10'),
('6e8de910f34e761a94eaf96a6e313df2eee0d2d9084e197c58aeb84b068575e9fb51e971a25976ae', 44, 1, 'Personal Access Token', '[]', 0, '2020-04-06 21:17:56', '2020-04-06 21:17:56', '2021-04-07 00:17:56'),
('71ae1d4d2f547a6fe2c2235d415ef00dee1fd2787b0214bc12ab62330ed444095fb085f2e59884f9', NULL, 1, 'Personal Access Token', '[]', 0, '2020-04-04 09:37:41', '2020-04-04 09:37:41', '2021-04-04 12:37:41'),
('729287cd643662eb35f9cf4ac8029af862ba85ec4a150893bedfb99769b8b9cf5448b5ad22e0970e', 44, 1, 'Personal Access Token', '[]', 0, '2020-04-07 08:09:21', '2020-04-07 08:09:21', '2021-04-07 11:09:21'),
('7537b911c2fc3a4de486e4da08e9dda9be88deeb4ed5d2702829a6bdc35c68e2a17c6cd2d1e7cd6a', 44, 1, 'Personal Access Token', '[]', 0, '2020-04-07 04:03:42', '2020-04-07 04:03:42', '2021-04-07 07:03:42'),
('7a7fbfa2a0ba0dd310e1b9de96dd150aec154c5e4f7f33444c87d60e3d76056bc3de0982ae791e9e', 47, 1, 'Personal Access Token', '[]', 0, '2020-04-07 04:04:09', '2020-04-07 04:04:09', '2021-04-07 07:04:09'),
('858d21837397bfc06af61b16d5320a6f1bedd8e41f4c82c0d36156d89b1f3d03f874c77cdb13ef31', 44, 1, 'Personal Access Token', '[]', 0, '2020-04-06 21:20:36', '2020-04-06 21:20:36', '2021-04-07 00:20:36'),
('8868c2a0654b313c6fff070c02fd9d9bbda8b5418f3641462046976baf4dea7eae939d6afed9250d', 30, 1, 'Personal Access Token', '[]', 0, '2020-04-03 08:49:37', '2020-04-03 08:49:37', '2021-04-03 11:49:37'),
('8a77e4e3e5a1cf3cb6c632d691d9fe29f05dc9ab416e5e67001eb9b0854581e10a1d99f622bf939e', NULL, 1, 'Personal Access Token', '[]', 0, '2020-04-04 10:05:21', '2020-04-04 10:05:21', '2021-04-04 13:05:21'),
('915151583e348f55cbf171fa0ed096243c2eedce214a6f90faf8b162941462e0144ea92b88a03bd4', NULL, 1, 'Personal Access Token', '[]', 0, '2020-04-04 11:02:07', '2020-04-04 11:02:07', '2021-04-04 14:02:07'),
('97d0e15f9fb5f87af155fda1ecdab597865ca4ede67fc09c83b292d02fe3d7b622dacb12c0cf7645', 44, 1, 'Personal Access Token', '[]', 0, '2020-04-08 13:26:41', '2020-04-08 13:26:41', '2021-04-08 16:26:41'),
('9a37d8efb45dabe21f8744d8f065039105997191e379f33e227d6b5d03db9302382d2be5c1e2c627', 8, 1, 'Personal Access Token', '[]', 0, '2020-04-02 08:35:58', '2020-04-02 08:35:58', '2021-04-02 11:35:58'),
('9e63ec3c6acfbcbac897282b66dc388f7a591531d8aee2d21a18a861cd2e198dbbfc69f0d86d1501', 8, 1, 'Personal Access Token', '[]', 0, '2020-04-02 07:58:41', '2020-04-02 07:58:41', '2021-04-02 10:58:41'),
('9ecc165dcbee324c0426074ede9e834cc47472e0f87242756b69cc946ad607a56681d55bc9a97bf7', 44, 1, 'Personal Access Token', '[]', 0, '2020-04-07 04:10:17', '2020-04-07 04:10:17', '2021-04-07 07:10:17'),
('a0cbb4712db3d4f47f8d24c5cdd985b0ba373f2b4db9b7f25183d2130a8ebeb6ada6369853416147', 44, 1, 'Personal Access Token', '[]', 0, '2020-04-06 21:14:16', '2020-04-06 21:14:16', '2021-04-07 00:14:16'),
('a31b69c524329dbfe036d3c2549befba393d322ae1011d39e8ab7adc4c225cf0bb86588fcec9079e', NULL, 1, 'Personal Access Token', '[]', 0, '2020-04-04 09:58:12', '2020-04-04 09:58:12', '2021-04-04 12:58:12'),
('a5454a279a106c62ed67a6d2a22d812946d0a19192143c696b4068c6946ea21694328bb4c0926806', 44, 1, 'Personal Access Token', '[]', 0, '2020-04-06 21:13:25', '2020-04-06 21:13:25', '2021-04-07 00:13:25'),
('a9ac430d80328e5b33ed93463516593771ba0b4fb0d3c8bc545584a24ee8d4f0d9bff3e26849c6d7', 44, 1, 'Personal Access Token', '[]', 0, '2020-04-06 21:19:18', '2020-04-06 21:19:18', '2021-04-07 00:19:18'),
('abf08663338a3c15d49ff64cc43b459087f01ad66c8ab3213c57f0ccbd91e3995f6f1c57b188ce53', 44, 1, 'Personal Access Token', '[]', 0, '2020-04-06 21:19:41', '2020-04-06 21:19:41', '2021-04-07 00:19:41'),
('b25d21248a209a93df6e092ff903b9877feacaddae7f9422e0887a2e66fd32ac881f1b3ed64c9a5d', 8, 1, 'Personal Access Token', '[]', 0, '2020-04-02 08:28:41', '2020-04-02 08:28:41', '2021-04-02 11:28:41'),
('b47770766fc5f81a5c05886ffc5be109de590026265e54d68f420ef4afe2a6e05dae99ce5872096e', 8, 1, 'Personal Access Token', '[]', 0, '2020-04-02 08:30:30', '2020-04-02 08:30:30', '2021-04-02 11:30:30'),
('b8dbc222c589e5974cad88b0e7d77746e614451f9b04c1e8d392deaf032f21ad150e45ebd7261544', NULL, 1, 'Personal Access Token', '[]', 0, '2020-04-04 10:02:48', '2020-04-04 10:02:48', '2021-04-04 13:02:48'),
('b8e820b51ded9d8d4fba03e0c91123beca4e082b3b4b88ffc9b37f43a448eca25ca1e602aad0af3b', 8, 1, 'Personal Access Token', '[]', 0, '2020-04-02 08:47:52', '2020-04-02 08:47:52', '2021-04-02 11:47:52'),
('c0766830ce0eac999c960f09b66b5863f3a08d10dc64de7bf1c237c8c01e542167a5ba3dbf7bbb74', NULL, 1, 'Personal Access Token', '[]', 0, '2020-04-04 10:02:27', '2020-04-04 10:02:27', '2021-04-04 13:02:27'),
('c546e441098028406f0ab6b1edc531b69c88e1277be1e71495ca97abf7121fcf0b4fe55f3a684de2', 44, 1, 'Personal Access Token', '[]', 0, '2020-04-06 21:13:38', '2020-04-06 21:13:38', '2021-04-07 00:13:38'),
('c774e91731b6950642d27ab1ff0079c84dfab5d4bd988efc7a2ceabc5890d34dd3acdb1d62a944ee', 8, 1, 'Personal Access Token', '[]', 0, '2020-04-02 08:16:10', '2020-04-02 08:16:10', '2021-04-02 11:16:10'),
('c8df6f9cf55dcb176efa8580842b019dfed34df5f4d832353ec07f65aacc93c826b2f6a56554c1e8', 44, 1, 'Personal Access Token', '[]', 0, '2020-04-06 21:16:39', '2020-04-06 21:16:39', '2021-04-07 00:16:39'),
('cdc8f4e650ccb33685998e079bdc6c203fd28eec19fb31ab4570d4a2368fc83e9f8d61283c5672a5', NULL, 1, 'Personal Access Token', '[]', 0, '2020-04-04 10:01:36', '2020-04-04 10:01:36', '2021-04-04 13:01:36'),
('cfb4e74788ecad974e88fa6a8f7d5c77df8a9c1f935d64cea68cacad37f60223fc5b833bd215173f', 44, 1, 'Personal Access Token', '[]', 0, '2020-04-06 21:18:14', '2020-04-06 21:18:14', '2021-04-07 00:18:14'),
('d6d890b1820e1690134b3cae0cb015241903d9d1d1ff02cda209f426a1712861d6f699167fd6f1f6', NULL, 1, 'Personal Access Token', '[]', 0, '2020-04-04 10:03:02', '2020-04-04 10:03:02', '2021-04-04 13:03:02'),
('dbd7f148333b3c6912abd440c6f2445463245cdba6a06d5ae95cbd6c963555aa6679a543c73d1d95', 44, 1, 'Personal Access Token', '[]', 0, '2020-04-07 08:00:37', '2020-04-07 08:00:37', '2021-04-07 11:00:37'),
('e55da0344ca71cc46496d376d9d1aacf62068787f13407105cf37f78e94b39b2af17f1b564597825', NULL, 1, 'Personal Access Token', '[]', 0, '2020-04-04 10:20:05', '2020-04-04 10:20:05', '2021-04-04 13:20:05'),
('fa6004bb2f7183f56608309ba5ad95796755ff1302575b7c0754e85ba26793f0b84afd86af1eb574', 47, 1, 'Personal Access Token', '[]', 0, '2020-04-07 18:26:13', '2020-04-07 18:26:13', '2021-04-07 21:26:13'),
('fb2d5f9c34bcde380f323bc344e71455ed6ae46c9ec69caa356e14fef3ae205012d5e12af210b53f', NULL, 1, 'Personal Access Token', '[]', 0, '2020-04-04 10:01:03', '2020-04-04 10:01:03', '2021-04-04 13:01:03');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', '2kpJAK7a0Vhg2tnPKi8rD2hZxg73TtfDScj9RkWu', 'http://localhost', 1, 0, 0, '2020-04-02 04:51:07', '2020-04-02 04:51:07'),
(2, NULL, 'Laravel Password Grant Client', 'zIrqq9EGSdGqEI0MGLI2KDm5BqgX7CY82wtc3bkD', 'http://localhost', 0, 1, 0, '2020-04-02 04:51:07', '2020-04-02 04:51:07');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-04-02 04:51:07', '2020-04-02 04:51:07');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `api_token` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `identification_number` varchar(9) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `api_token`, `remember_token`, `created_at`, `updated_at`, `identification_number`, `type`) VALUES
(1, 'xZ4N4rLH', 'TCkxrEeI0kxU@mail.com', NULL, '$2y$10$CXmZUmxu43Uy/RqXTQk8xeVRLBST52gHYAkD51MQe6h7miRyO9URm', 'HpCuRQD648ufJK5pc9AMDfjx3l10O3xbS3xlK7rflySPak8TWniKLV2ZQqeoNrI7cJxdyZ2m6mgWla81', NULL, '2020-04-04 12:20:25', '2020-04-04 12:20:25', '647008945', 'default'),
(2, 'Doron Baisa2', 'TF0GT24Yor1Z@mail.com', NULL, '$2y$10$BJfQaxY1d58KjvlrTSHM.uXp7ObW3.x22QRCbsVOoqSKQh5aGxamC', 'ppOf6wP3gZ5ElNZ1tjUidz4Qlg0RXUdXYq9EXB0cek9kvVkTuJ2kTnuz95obGlVxIFtDREj73bYNIOzI', NULL, '2020-04-04 12:20:25', '2020-04-04 12:20:25', '145171473', 'default'),
(3, 'hqOFTlJ4', 'rumJYBYr2J60@mail.com', NULL, '$2y$10$FJWlLSyDUwt1IEIcAz7omuHWORZOeZ10NeQEbdox5hWBDBVfDFFnK', 'UEuzQC66gUAULi2HhCktFEuKp28SwPk9Irucw04d9WGrIcULDxobc1w3pQcUKmzBwB5PrZrB4bwVMomA', NULL, '2020-04-04 12:20:25', '2020-04-04 12:20:25', '243009803', 'default'),
(4, 'FsyRkxZu', 'd40ubGFt2iHk@mail.com', NULL, '$2y$10$LfL1kRa02uMivTL/EOHQU.yT/wzJuxSX4I6Jj65df2aFRPv5QIiWm', '3AwUNPet5pBExgWjMEp3uKG5NuK8tbB3yduNQFX7BJxoBr2npDyFvvbytoqo9QR7dZkaj4bNTLLuaQEw', NULL, '2020-04-04 12:20:25', '2020-04-04 12:20:25', '158742882', 'default'),
(5, 'wPXAmeWZ', 'gF6vZKqdrB3J@mail.com', NULL, '$2y$10$DqAfEUL99agFbNs.BrzIde5lyjvtIhO063YVCW5n.ZujSdYaTjBAy', 'TQEliNCi7h8O0GQj0x5PMAaxW15TbgrfJgv59iZJ7F47LUUQgFyalHvoVsXrX1Q04NC0Pj1NyCkqeIHY', NULL, '2020-04-04 12:20:25', '2020-04-04 12:20:25', '381622792', 'default'),
(6, 'QK34ABuZ', 'yEkoBbmh2C2j@mail.com', NULL, '$2y$10$wObLl3qME4XdswGtP2nF2ulpeM4xNAda1RCdNVkcpbc.RjTq1An6W', 'WuO5GKcNINqSJqRVgxU13aFD33r4AOKKRy1FM2VnxqETVIOFVJMpw9f8eMk7pGgf5wslN5ikgGk4H5Sw', NULL, '2020-04-04 12:20:25', '2020-04-04 12:20:25', '220472678', 'default'),
(7, 'Doron Baisa', 'CDJMsIzVbPf0@mail.com', NULL, '$2y$10$qaI7N5NYc3ntVA73cT3.su/7Qe0Hw.Nh.iNKY.C9fd2WZPukcOvKa', '8TYwsokoYWmlOFW63IWsSCCQ8Dx10HpyICtnKtrvNteSFdEMz1isJfy0hVSnqpsZyRjzBrlysgUoCLjA', NULL, '2020-04-04 12:20:25', '2020-04-04 12:20:25', '569550176', 'default'),
(8, 'vtKiMh8R', 'yJckPVS86suq@mail.com', NULL, '$2y$10$R5yN/bOBhWLPobw28HLouOIJPzXzfMdn5ZS7XAG87kr01B8N4z.6u', 'lv55oZVDy1JmQYKnqMfmCVcAKqZCc3JDQbpIFjDu8baWdjrdCMIe4j5GFkptV5XSukTwGlqAZ65jKoOr', NULL, '2020-04-04 12:20:25', '2020-04-04 12:20:25', '482197029', 'default'),
(9, 'E59xmPyb', 'BwC8wy6tkh7r@mail.com', NULL, '$2y$10$jVV/SWcMtudtPDezEcnRduCUxncC8xyWHyNxpFd08z0JFob2E3VKa', 'BbdiXqXBbfaOYqxaT6TFUyz6ZmIWlFjcX6ESixG6NoneJ9TxQTJj6E6HBf2zCrqYt01kHBVYDQ0UidUf', NULL, '2020-04-04 12:20:25', '2020-04-04 12:20:25', '734778291', 'default'),
(10, '8Wi7bAVY', 'rBAsEueU46Um@mail.com', NULL, '$2y$10$V8dsKZS/VKQU./uxQR6qn.NarfVeFhQ/S0/aZIs6zm02rIPWxPsZa', 'MoVZk0aniHu10p1AWP3ft0OZJr0ofSwtGVyMrp5tHd1olwTxqJC9yg9F7V38MfR8q7TH3ODdWA3sDEZK', NULL, '2020-04-04 12:20:25', '2020-04-04 12:20:25', '198887526', 'default'),
(11, 'BIKCeIKC', 'weYc8jPvoJQa@mail.com', NULL, '$2y$10$gifEKFrr.BVzGAdPNkMMHOROcpy9wMVhnLP2zcvfPYaOWSS.u9koW', 'Bag4gFZiEV8off8JuZbit2Ctt9JFWv3bWN6blHfznXuNYftHDTgIxAywMbq1GfujeMS46EOYdMzOLHzN', NULL, '2020-04-04 12:20:25', '2020-04-04 12:20:25', '434447543', 'default'),
(12, 'wrl8TQxc', 'H2Hh644jx3BW@mail.com', NULL, '$2y$10$LZdSDfAuL7p02MDJzwwPlOJPQ0bTHEEyo.2E39Ca18yUeGvxuV37G', '4woo4LQRrGgLgfV4L6QbV4bAE5DBoBNKGLWOtFIsaRRVIxNjaako9Fk5PIhbzonsBHkV0TUZu316euSg', NULL, '2020-04-04 12:20:25', '2020-04-04 12:20:25', '823669805', 'default'),
(13, 'xCFKXGqV', 'ibY7BWvYFN6T@mail.com', NULL, '$2y$10$0cyZchBLi06hCAVJsZzzZuM7nWLs9q2VNw99mGKEqdJaZ4dzqyQ7W', 'Iam5NROP0sPl8Cst65r8wlbOQUqfCpVVi1A0t08qpUF5zP5XmkjrEaguBHP7AimDHcH2DYYyeGVRC5zY', NULL, '2020-04-04 12:20:25', '2020-04-04 12:20:25', '477815387', 'default'),
(14, 'JPqgq140', 'k6IZqRYUyAPQ@mail.com', NULL, '$2y$10$UvlkD3thic28NUXx2hrh9u9SSwUhGvek/Wjqidn943MOSu90Cois2', 'wlCBWs8WfPwY7cQ8Qk4pqltjmEWsp0XA8BCrosq4d5XXRZxYlPkPe3tThURkzj0MFm08Af7xn2824hbQ', NULL, '2020-04-04 12:20:25', '2020-04-04 12:20:25', '210986545', 'default'),
(15, 'c5pTorFG', 'D3ZaB1yfyW5v@mail.com', NULL, '$2y$10$qsP/b1woVT0EZnvFFybUnujcuRKfWEKwghshxt2Eo9DUMC6vtCnJi', '2foMRBPHN02q0Lm5EqVX6Q1y7jeJVAgjxmQri13EbO9DERi5BK7gc6CjYbMJXbNeqm5aUMu8KdYV3cYs', NULL, '2020-04-04 12:20:26', '2020-04-04 12:20:26', '678054364', 'default'),
(16, 'i0Px2CUR', 'AsElEsNJgHQz@mail.com', NULL, '$2y$10$Djjuh8hKrNWiZB3OlAEWTePEG7kdZOzyz4/JH/PdZZm8IhVIFVvS6', 'bjLlAmjX0s8xxWdsPGFyFlFOdfoCrKl6CLcd7WawjPMaagPoZk3Al8WjejM9lDv2jQxG33HwdCbUPqZd', NULL, '2020-04-04 12:20:26', '2020-04-04 12:20:26', '794706012', 'default'),
(17, 'QkpRWDj1', 'bZZ4MYkMWh06@mail.com', NULL, '$2y$10$rWTX1NYyvOXAfpf6aVfBq.6S8uxZySPSfO2ugxqfuyJ3erJIc4EU6', 'fm1I5hWaWHa7T90u7XAFuiKjwsBvnu9LDWknpCB4BIIHmSb6Cl0SMbOM3gajFlfkbdJAhlHFe0PdKrTE', NULL, '2020-04-04 12:20:26', '2020-04-04 12:20:26', '357004361', 'default'),
(18, 'eee123', 'Z5E4Wxt325Zg@mail.com', NULL, '$2y$10$wx8A6e1JAEbhkLRT2OTAaumjYCRzy19Ea066lqtWnGWq6KENLztA2', '3l0ZAnYfNz75dlQE93yaINAJbgCLg8lroYO2bD2OZEHvj5oXqAHxtbndUt9oa0HkIBLGBWlaXKdZvWkG', NULL, '2020-04-04 12:20:26', '2020-04-04 12:20:26', '207569024', 'default'),
(19, 'wdTKMlK6', 'SC8zt5NTiJAj@mail.com', NULL, '$2y$10$G3jOUndnFUtoCbAzBBgBx.cyPEsDR9cw235vLZdwRCsNgaXo5e0pa', 'zDscMzE6MXe3peKYDAaY7Yerrf6N02UEJgk7e2WtRMW81egMKQ4PGmi7rXqvCBpt38FESGGrtlqNxsoV', NULL, '2020-04-04 12:20:26', '2020-04-04 12:20:26', '620062168', 'default'),
(20, 'vGh7pTI2', '0vmPzFtFLUrZ@mail.com', NULL, '$2y$10$cbHO5TKq4pl/LS3w90S1S.WrGzue8Ff66oQ9eObXKdTaglI9BJTOK', 'AWsiKhTNqr7UukhMgaSKzTkgRrkqwRdAzViBQQ7b5jWbsrqilF7iq8eSFGSYeVLHNnPO7YZyXXQitMDv', NULL, '2020-04-04 12:20:26', '2020-04-04 12:20:26', '894987434', 'default'),
(21, 'iDEH5Kcv', 'DQSeTdIe7eRp@mail.com', NULL, '$2y$10$9Ibx4/QA4bWdPUvP1/IcvOAvNpdTNEu44Ulbj6juGVEbTfCkhnrfS', 'igHcnNGcTHyk66sO7MrYO1InBzYX2zgCBf3lKrUdp9igEgBesfzjleA0HQUsVYPsMTA7KIJayfSajkF0', NULL, '2020-04-04 12:20:26', '2020-04-04 12:20:26', '482814721', 'default'),
(22, '2eSRp0Nv', 's1UbqnIheIFs@mail.com', NULL, '$2y$10$8.pyopm/pmKKWzZ44zWaB.vw69K.Iv2MrQEdIQBFQXsqDN81HGlGe', 'tgl2Ka1Dv82fpD14cmSukpG6KtVf4RthNhf67GonBY7C8JQhuyr5XGQJFeP9NasXILb0x6jXDeY9iT3B', NULL, '2020-04-04 12:20:26', '2020-04-04 12:20:26', '663368737', 'default'),
(23, '35CH1qxl', '6JZFcYPaovyu@mail.com', NULL, '$2y$10$qIf52JEuuAro.Z25j6R4..QrpWj.HhUmU4KedkkcCuxW20FXKEdmG', 'kRIw0oSD9c9bbrhyQdAoKFpaRjmjPa3GxLzhkmfULr4Q8QshLK7zQ9vCRFbI5ixfsJ1Td6jE2KN2drL5', NULL, '2020-04-04 12:20:26', '2020-04-04 12:20:26', '883710083', 'default'),
(24, 'Be2deIt3', 'J41SimIctUcJ@mail.com', NULL, '$2y$10$ZBi.Wvr0nyoVU3hU9uh.GeWblmgDTnE9JoCupc8s0cwYWtmdZrb6C', '9Uu8BxRlaWvRnaUStRf4WsgJuQyfmP2u760l6npPUf1d5uVhWeXuo75USn3e1vY4BPl8XwmXZycJjlrV', NULL, '2020-04-04 12:20:26', '2020-04-04 12:20:26', '197210118', 'default'),
(25, 'u2ZAW5vq', 'GyJIkyie7to0@mail.com', NULL, '$2y$10$5iTZJ/A48w0te9j3kLWs/eYhFkiPFyPJn5D4QU7hoZF2cIuJfXGMK', 'McJNtI5Qo6nCUmqPlADuffZ6Q7R1zxE9QdhNm7FLwdryqYJd8zFhhPLHkT1SlO37fEhfE5jPZJ4ecLqk', NULL, '2020-04-04 12:20:26', '2020-04-04 12:20:26', '552054744', 'default'),
(26, 'Jtb7XvMM', 'JvSoA2ZmEpVT@mail.com', NULL, '$2y$10$3Md41tVEMcRFwxRmhkYeDeFMMSp8boI4soVz332y1drjdWO/q2OO6', 'pxstJxDROVE5Z7CJfD1y1F8pi5yvPZ4sy74iIU2DNR2EgNn1bdovD58Vbqg9ABzBZJl2v4eb6fNqLFmr', NULL, '2020-04-04 12:20:26', '2020-04-04 12:20:26', '354733267', 'default'),
(27, 'joKkwUFt', 'qUu8nGq1wzRP@mail.com', NULL, '$2y$10$O1C3fAHGIK38CKfVPaWen.lm8XSH8HstXvZ8bak.CTbktyF1TTxPC', 'fTC3njLawqGknr5EPoDqr9D0LkmhBelH3B0lg5WPE1Fwq3zHO2dvRBFQ6oHwDcKmSJBU4ZfhYNMYI2LF', NULL, '2020-04-04 12:20:26', '2020-04-04 12:20:26', '717560059', 'default'),
(28, 'vNhOcYqC', 'JRtl44I3M9En@mail.com', NULL, '$2y$10$FSGDNR.8Vj2qgfheAm7ReukOMn/oZRVsjBhO8GBJxoioslk3b.uLa', 'Ia0LriqoGfLWzTbRrfnJGJd6JrugYuWZ57ySUBoJWabqxywkGqTpKiXp254z8mcofQcORpMr6Uy6IFZx', NULL, '2020-04-04 12:20:26', '2020-04-04 12:20:26', '436319817', 'default'),
(29, 'zOMMDIYc', 'ZFrs8ySllbV5@mail.com', NULL, '$2y$10$iLMgwsOCV6st.0v1EbnDHeYvVVYcaja2yVQDSbVOOlSB0t2UCZOj6', 'Zlq49mPAPDBzeJYbH0hmGTTtZ7JxJhxKFjqavjV5T9tOkO5FvLEmg9epO58jr3bJbtqwUPsmwSyIF3sz', NULL, '2020-04-04 12:20:26', '2020-04-04 12:20:26', '776120262', 'default'),
(44, 'Doron Baisa', 'admin@gmail.com', NULL, '$2y$10$j/mR728s2TWx0m4H87ILFeBoow5v2WUK4gkd8jdZ8UeIfjageDpKO', 'Zlq49mPAPDBzeJYbH0hmGTTtZ7JxJhxKFjqavjV5T9tOkO5FvLEmg9epO58jr3bJbtqwUPsmwSyIF3s3', NULL, '2020-04-04 12:22:49', '2020-04-04 12:22:49', '', 'admin'),
(60, 'Doron Baisa', 'admin11@gmail.com', NULL, '$2y$10$heWzdQ44q5w7SV8Q6Y3/l.FLyBZ8aVA36sc3RbiZ0hiIm.ZXyu4ze', 'PJrKGNaDyx0fx5nVdI5QL2mEUsj2lG9wmjZtV4jSdHO4bUbVhkcbdw6IarPuMQpxAAVbwN7rRBN4n7ip', NULL, '2020-04-07 19:53:12', '2020-04-07 19:53:12', '532245363', 'default'),
(61, 'Doron Baisa', 'student@gmail.com', NULL, '$2y$10$KDbTpfR6GjCBy.8t90EaWuT9oEIaGGsdg4mgZ/UdoVokvUZauTrXq', 'K9qjUIokRGFxZ8zHPDh0OFHKENnFJQH9GsASjZK6mToSfnY8EHmLFn5it9P114eE32NUURD8aKB4QOlb', NULL, '2020-04-08 12:52:22', '2020-04-08 12:52:22', '728729359', 'default');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `exams`
--
ALTER TABLE `exams`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_api_token_unique` (`api_token`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `exams`
--
ALTER TABLE `exams`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=170;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
