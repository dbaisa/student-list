import React from 'react';
import ReactDOM from 'react-dom';
import Cookies from 'js-cookie';
import TableUsers from './TableUsers';


import $ from 'jquery';
 
// Component's Base CSS
import './index.css';
const baseUrl = 'http://127.0.0.1:8000';
let token = window.$('meta[name="csrf-token"]').attr('content');
window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token
axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
axios.defaults.headers.common['Authorization'] =  'Bearer ' + Cookies.get('access_token');

  
class Student extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            DescriptionRenderer:  ({ field }) => <textarea {...field} />,
            rows: [],
            options: '',
            userId: ''
        }
        this.getGrades = this.getGrades.bind(this);
    }
    getGrades(){
        
        axios.get(`${baseUrl}/api/auth/exams_by_user`)
        .then(response => {
            let data = response.data.map((val) => {
                return {
                        id: val.id,
                        grade:val.grade,
                        proffession: val.proffession,
                        examid: val.examid
                    }
            });

            this.setState({
                rows: [...data]
            });
          
        });
    }
    componentDidMount(){
        this.getGrades();
    }
   
    render(){
        return(
               <TableUsers rows={this.state.rows} />
        )
    }   
}
// AdminTable.propTypes = {};
export default Student;