import React from 'react';
import ReactDOM from 'react-dom';
import Cookies from 'js-cookie';
import CRUDTable,
{
  Fields,
  Field,
  CreateForm,
  UpdateForm,
  DeleteForm,
  Pagination,
} from 'react-crud-table';

import $ from 'jquery';
 
// Component's Base CSS
import './index.css';
const baseUrl = 'http://127.0.0.1:8000';
let token = window.$('meta[name="csrf-token"]').attr('content');
window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token
axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
axios.defaults.headers.common['Authorization'] =  'Bearer ' + Cookies.get('access_token');

const SORTERS = {
    NUMBER_ASCENDING: mapper => (a, b) => mapper(a) - mapper(b),
    NUMBER_DESCENDING: mapper => (a, b) => mapper(b) - mapper(a),
    STRING_ASCENDING: mapper => (a, b) => mapper(a).localeCompare(mapper(b)),
    STRING_DESCENDING: mapper => (a, b) => mapper(b).localeCompare(mapper(a)),
};
  
 
const graade = ({ field }) => <input { ...field } type="number" min="0" />;
 const styles = {
    container: { margin: 'auto', width: 'fit-content' },
  };
  
class AdminTable extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            DescriptionRenderer:  ({ field }) => <textarea {...field} />,
            rows: [],
            count: 0,
            number: ({ field }) => <input { ...field } type="text" maxLength="9" />,
            selectUsers: ({ field }) => <select { ...field }>{this.state.options}</select> ,
            options: '',
            proffessions: ({ field }) => <select { ...field } >{this.state.proffessionOptions}</select>,
            proffessionOptions: '',
            crudTable: '' 

        }
        this.getGrades = this.getGrades.bind(this);
        this.getSorter = this.getSorter.bind(this);
        this.fetchItems = this.fetchItems.bind(this);
        this.renderCrud = this.renderCrud.bind(this);
        this.myFilter = this.myFilter.bind(this);
        this.create = this.create.bind(this);
    }
    onlyUnique(value, index, self) { 
        return self.indexOf(value) === index;
    }
    getGrades(){
        let options = [];
        axios.get(`${baseUrl}/api/auth/get-all-users`)
        .then(response => {
            options = response.data.map((val) => {
            return <option value={val.id}>
                            name: {val.name} ,
                            identification_number: {val.identification_number} 
                    </option>

            });
            options.unshift(<option></option>);
        })
        axios.get(`${baseUrl}/api/auth/exams`)
        .then(response => {
            let data = response.data.map((val) => {
                return {
                        id: val.id,
                        examid: val.examid,
                        grade:val.grade,
                        student_id: val.id,
                        name: val.name,
                        identification_number: val.identification_number,
                        proffession: val.proffession
                    }
            });
           
            
            const proffessions = response.data.map((val) => {
                return val.proffession
            });
            let proffessionsUnique =  proffessions.filter(this.onlyUnique);
            let proffessionOptions =  proffessionsUnique.map((val) => {
                return <option value={val}>
                                {val} 
                        </option>
            });
            proffessionOptions.unshift(<option></option>);
            
            this.setState({
                options,
                proffessionOptions
            })
            this.setState({
                rows: [...data]
            }, () => {
                this.setState({
                    crudTable: this.renderCrud()
                });
            })
        });
    }
    componentDidMount(){
        this.getGrades();
    }
    async createInBackend(data){
        let info = [];
          let response = await axios.post(`${baseUrl}/api/auth/exams/create`,{
            name: data.name,
            id: data.id,
            identification_number: data.identification_number,
            grade: data.grade,
            student_id: data.id,
            proffession: data.proffession
        })

        return response.data[0];
        // .then(response => {
        //     info = response.data.map((val) => {
        //         return {grade:val.grade,
        //                 student_id: val.id,
        //                 name: val.name,
        //                 identification_number: val.identification_number,
        //                 proffession: val.proffession,
        //             }   
        //     });
        //     this.setState({
        //         rows: [...info]
        //     })
        // })
    }
    async create(data){ 
        let newData = {};
        //let newData = this.state.rows.find(row => row.id == data.selected_student);
       // console.log(newData);
        newData.id = data.selected_student
        newData.proffession = data.proffession;
        newData.grade = data.grade;
        
        let response = await this.createInBackend(newData);
        console.log(response);
        newData.examid = response.examid;
        newData.name = response.name;
        newData.identification_number = response.identification_number;
        let tempArr =  [...this.state.rows];
        tempArr.unshift(newData);
        this.setState({
            rows: [...tempArr]
        },this.setState({
            crudTable: this.renderCrud()
        })) 
    
        

        return Promise.resolve(data);
    }
      async update(data){
        let info = [];
        await axios.post(`${baseUrl}/api/auth/exams/update`,
            {   
                examid: data.examid,
                name: data.name,
                student_id: data.student_id,
                identification_number: data.identification_number,
                grade: data.grade,
                proffession: data.proffession
            }
        ).then(response => {
             info = response.data.map((val) => {
                return {
                        examid: val.examid,
                        grade:val.grade,
                        student_id: val.id,
                        name: val.name,
                        identification_number: val.identification_number,
                        proffession: val.proffession 
                    }
            });
            this.setState({
                rows: [...info],
                
            },this.setState({
                crudTable: this.renderCrud()
            }))
        });

        return Promise.resolve(info);
      }
        deleteInBackend(data){
            axios.post(`${baseUrl}/api/auth/exams/delete`,
                {
                    examid: data.examid,
                }
            );
        }
    async delete(data){
        this.deleteInBackend(data); 
        let task = this.state.rows.find(t => t.examid === data.examid);
        let tasks = this.state.rows.filter(t => t.examid !== task.examid);
        this.setState({
            rows: [...tasks],
            
        },this.setState({crudTable: this.renderCrud()}))
        return Promise.resolve(task);
      }
 
    getSorter(data){
        const mapper = x => x[data.field];
        let sorter = SORTERS.STRING_ASCENDING(mapper);
        
        if (data.field === 'id') {
            sorter = data.direction === 'ascending' ?
            SORTERS.NUMBER_ASCENDING(mapper) : SORTERS.NUMBER_DESCENDING(mapper);
        } else {
            sorter = data.direction === 'ascending' ?
            SORTERS.STRING_ASCENDING(mapper) : SORTERS.STRING_DESCENDING(mapper);
        }
        
        return sorter;
    };
    myFilter(element, condition ,field, value) {
        if(condition == 'CONTAINS'){
            return element[field].indexOf(value) !== -1;
        }
        if(condition == 'EQUALS_TO'){
            return element[field] === value;
        }
        if(condition == 'ENDS_WITH'){
            return element[field].endsWith(value);
        }
    }
    fetchItems(payload){
        let result;
        let { activePage, itemsPerPage } = payload.pagination;
        let {field,condition,value} = payload.queryRules[0] ? 
                                        payload.queryRules[0] : {condition:'',value:'', field:''};
        activePage = activePage ? activePage : 1;
        itemsPerPage = itemsPerPage ? itemsPerPage : 2;
        const start = (activePage - 1) * itemsPerPage;
        const end = start + itemsPerPage;
        result = Array.from(this.state.rows);
        result = condition ? result.filter((row) => {return this.myFilter(row,condition,field,value)}) : result;
        //result = result.sort(this.getSorter(payload.sort));
        return Promise.resolve(result.slice(start, end));
    }
    fetchTotal(){
        return Promise.resolve(this.state.rows.length);
    }
    renderCrud(){
       return (
            <CRUDTable
           caption="Grades"
           fetchItems={payload => this.fetchItems(payload)}
           showQueryBuilder={true}                
           
           >
           <Fields>
             
               <Field
                   name="selected_student"
                   label="Select A Student"
                   render={this.state.selectUsers}
                   queryable={false}
                   hideInUpdateForm
               />
               <Field
                   name="examid"
                   label="Exam ID"
                   //render={this.state.selectUsers}
                   queryable={false}
                   hideInUpdateForm
                   hideInCreateForm
                   hideInDeleteForm
               />
               <Field
                   name="identification_number"
                   label="Student ID"
                   render={this.state.number}
                   hideInCreateForm
                   queryable={true}
                   sortable={true}
               />
               <Field
                   name="name"
                   label="Name"
                   placeholder="name"
                   hideInCreateForm
                   
               />
               <Field
                   name="grade"
                   label="Grade"
                   render={graade}
                   queryable={true}
                   sortable={true}
               />
               <Field
                   name="proffession"
                   label="Proffession"
                   render={this.state.proffessions}
                   queryable={true}
                   sortable={true}
               />
           </Fields>
           
           <CreateForm
               title="Grade Creation"
               message="Create a new grade!"
               trigger="Create A New Grade"
               onSubmit={task => this.create(task)}
               submitText="Create"
               
            //    validate={(values) => {
            //    const errors = {};
            //    if (!values.selected_student) {
            //        errors.title = 'Please, Choose a student';
            //    }
       
            //    if (!values.grade) {
            //        errors.title = 'Please, provide a grade';
            //    }
               
            //    if (!values.proffession) {
            //         errors.title = 'Please, provide a proffession';
            //     }
            //    return errors;
            //    }}
           />
       
           <UpdateForm
               title="Task Update Process"
               message="Update task"
               trigger="Update"
               onSubmit={exam => this.update(exam)}
               submitText="Update"
               // validate={(values) => {
               // const errors = {};
       
               // if (!values.id) {
               //     errors.id = 'Please, provide id';
               // }
       
               // if (!values.title) {
               //     errors.title = 'Please, provide task\'s title';
               // }
       
               // if (!values.description) {
               //     errors.description = 'Please, provide task\'s description';
               // }
       
               // return errors;
               // }}
           />
       
           <DeleteForm
               title="Delete student grade"
               message="Are you sure you want to delete the grade?"
               trigger="Delete"
               onSubmit={grade => this.delete(grade)}
               submitText="Delete"
            //    validate={(values) => {
            //    const errors = {};
            //    if (!values.id) {
            //        errors.id = 'Please, provide id';
            //    }
            //    return errors;
            //    }}
           />
          <Pagination
               itemsPerPage={10}
               fetchTotalOfItems={payload => this.fetchTotal(payload)}
           />
           </CRUDTable> 
        
       );
    }
    render(){
        //this.getSelectUsersOptions();
        return(
            <div style={styles.container}>
                {this.state.crudTable}
            </div>
        )
    }   
}
// AdminTable.propTypes = {};
export default AdminTable;