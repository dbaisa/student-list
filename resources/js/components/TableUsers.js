/* eslint max-len: 0 */
import React from 'react';
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';
// import 'bootstrap/dist/css/bootstrap.min.css';
// require('react-bootstrap-table-next/dist/react-bootstrap-table2.min.css');

// var products = [{
//   id: 1,
//   proffession: "Item name 1",
//   grade: 100
// },{
//   id: 2,
//   proffession: "Item name 2",
//   grade: 100
// }];


 
const columns = [{
  dataField: 'id',
  text: 'Product ID',
  sort: true
}, 
{
  dataField: 'examid',
  text: 'Exam ID',
  sort: true
},
{
  dataField: 'proffession',
  text: 'Proffession',
  sort: true
}, {
  dataField: 'grade',
  text: 'Grade',
  sort: true
}];



const sizePerPageOptionRenderer = ({
  text,
  page,
  onSizePerPageChange
}) => (
  <li
    key={ text }
    role="presentation"
    className="dropdown-item"
  >
    <a
      href="#"
      tabIndex="-1"
      role="menuitem"
      data-page={ page }
      onMouseDown={ (e) => {
        e.preventDefault();
        onSizePerPageChange(page);
      } }
      style={ { color: 'red' } }
    >
      { text }
    </a>
  </li>
);

const options = {
  sizePerPageOptionRenderer
};
const defaultSorted = [{
  dataField: 'proffession',
  order: 'desc'
}];
class TableUsers extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    let { rows } = this.props;
    return (
      <div>
        <BootstrapTable  keyField='examid' data={ rows } columns={ columns }  pagination={ paginationFactory(options) }/>
      </div>
    );
  }
}
export default TableUsers;