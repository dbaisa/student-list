import React from 'react';
import ReactDOM from 'react-dom';
import AdminTable from './components/AdminTable';
import {BrowserRouter, Switch,Route} from 'react-router-dom';
import Student from './components/Student';

if (document.getElementById('root')) {
    ReactDOM.render(
            <BrowserRouter>
                <div>
                    <Switch>
                        <Route exact path="/:id/edit" component={TaskEdit}/>
                        <App />
                    </Switch>
                </div>
            </BrowserRouter>
            , document.getElementById('root'));
}


if (document.getElementById('admin')) {
    ReactDOM.render(
        <AdminTable />
            , document.getElementById('admin'));
}

if (document.getElementById('student')) {
    ReactDOM.render(
        <Student />
            , document.getElementById('student'));
}

