<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
    protected $fillable = ['grade' , 'student_id'];
    
    public function grade(){
        return $this->belongsTo(User::class);
    }
    public static function all($columns = ['*']){
        $allExams = DB::table('users')
        ->select('users.id, users.identification_number','users.name','exams.grade')
        ->join('exams','exams.student_id','=','users.id')
        ->get();
        return $allExams;
    } 
}
