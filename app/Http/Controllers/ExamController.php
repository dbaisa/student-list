<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Exam;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
class ExamController extends Controller
{

    public function __construct(){
        // $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Exam $exam)
    {
    
        $allExams = DB::table('users')
        ->select('exams.id as examid' ,'users.id','users.name','exams.grade','users.identification_number','exams.proffession')
        ->join('exams','exams.student_id','=','users.id')
        ->get();
        return $allExams;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function exams_by_user(Request $request, Exam $exam)
    {
        $userId = Auth::id();
        $allExams = DB::table('users')
        ->select('exams.id as examid' ,'users.id','users.name','exams.grade','users.identification_number','exams.proffession')
        ->join('exams','exams.student_id','=','users.id')
        ->where('users.id','=',$userId)
        ->get();
        return $allExams;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {   
        $input = $request->all();
        // $user = new User;
        // $user->name = $input['name'];
        // $user->identification_number = $input['identification_number'];
        
        // $user->email = Str::random(12).'@mail.com';
        // $tokenResult = $user->createToken('Personal Access Token');
        // $user->api_token = $tokenResult->accessToken;
        // // Set other fields ...
        //  $user->save();
        $exam = new Exam;
        // DB::table('exams')
        //          ->create(['student_id',$input['student_id'],'grade'=> $input['grade']]);
        $exam->student_id = $input['student_id'];
        $exam->grade =  isset($input['grade']) ? $input['grade'] : '0'  ;
        $exam->proffession =  $input['proffession'];
        $exam->save();
        // if(){
        //         return response()->json([
        //             'examid' => $exam->id,

        //         ]);
        // }
        // else{
        //     echo "faild to create a new exam!";
        // }

        $allExams = DB::table('users')
        ->select('exams.id as examid','users.id', 'users.identification_number','users.name','exams.grade','exams.proffession')
        ->join('exams','exams.student_id','=','users.id')
        ->where('exams.id' , '=', $exam->id)
        ->get();
        return $allExams;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request , [
            'grade' => 'required'
        ]);

        $grade = $request->student()->grade()->create([
            'grade' => $request->grade,
        ]);

        return response()->json($grade->with('student_id')->find($grade->id));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mark = Exam::findOrFail($id);
        return response()->json([
            'mark' => $mark
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $input = $request->all();
        DB::table('exams')
                ->where('student_id',$input['student_id'])->update(
                    ['grade'=> $input['grade'],
                     'proffession' => $input['proffession']   
                    ]
                );
        DB::table('users')
                ->where('id',$input['student_id'])->update(
                    ['identification_number'=> $input['identification_number'],
                     'name' =>$input['name']]);        
        $allExams = DB::table('users')
        ->select('exams.id as examid','users.id', 'users.identification_number','users.name','exams.grade','exams.proffession')
        ->join('exams','exams.student_id','=','users.id')
        ->get();
        return $allExams;

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $input = $request->all();
        DB::table('exams')
                ->where('id',$input['examid'])->delete();
        $allExams = DB::table('users')
        ->select('exams.id as examid','users.id', 'users.identification_number','users.name','exams.grade','exams.proffession')
        ->join('exams','exams.student_id','=','users.id')
        ->get();
        return $allExams;

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Exam::findOrFail($id)->delete();
    }
}
