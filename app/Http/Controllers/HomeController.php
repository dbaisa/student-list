<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {   
        $user = $request->user();
        if($user->type == 'admin'){
            return view('admin');
        }
        else{
            return view('home');
        }
    }
    public function users(Request $request)
    {   
        $allUsers = DB::table('users')
        ->select('users.id','users.name','users.identification_number')
        ->get();
        return $allUsers;

    }
}
