<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(['middleware' => ['auth:api'],'prefix' => 'auth'], function() {
    Route::get('exams', 'ExamController@index');
    Route::get('get-all-users', 'HomeController@users');
    Route::get('exams_by_user', 'ExamController@exams_by_user');
    Route::post('exams/update', 'ExamController@update');
    Route::post('exams/delete', 'ExamController@delete');
    Route::post('exams/create', 'ExamController@create');

});
Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');
  
    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');
    });
});