1. Install Node js from https://nodejs.org/en/ and after that restart your computer
2. Install composer from https://getcomposer.org/download/ and add it to your system PATH   
3. Download and install Xammpp from https://www.apachefriends.org/download.html
4. Run Xampp 
5. open  http://localhost/phpmyadmin/   and create a new DataBase
6. Install git on your system from  https://git-scm.com/downloads  
7. Open cmd and navigate to your htdocs folder inside xammp usually is in C:\xampp\htdocs\
8. Run the command Git clone https://dbaisa@bitbucket.org/dbaisa/student-list.git   
9. Download and install Visual Studio Code from   https://code.visualstudio.com/download  
10. Open Visual Studio Code program and then open folder  C:\xampp\htdocs\student-list by File->Open Folder
11. Open .env file and change the parameters DB_DATABASE DB_USERNAME DB_PASSWORD to your database credentials 
12. Use the shortcut control+~ and it will open a terminal inside the Visual Studio code.
13. Run on that Terminal "npm install"
14. Run "npm run watch"
15. Run the command composer install
16. open another terminal by clicking on the right-left + icon
17. Do Step 4 in https://www.sitepoint.com/how-to-install-php-on-windows/ and after that restart your computer
18. Run php artisan passport:keys
19. Run php artisan passport:client --personal
20. Run there the command "php artisan serve" 
21. open another terminal by clicking on the right-left + icon
22. Import sql code from the file C:\xampp\htdocs\studentlist.sql inside pypmyadmin in order to install the DB data.
23. Open a browser and navigate to http://127.0.0.1:8000/ and login with user email admin@gmail.com password:123456789
24. Open another browser in incognito mode and run  http://127.0.0.1:8000/ and login with user email student@gmail.com password:123456789 
